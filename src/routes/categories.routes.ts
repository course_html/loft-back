import express from 'express'
import categoriesController from '../controller/categories.controller'

const router = express.Router()

router.post('/categories', categoriesController.createCategory)
router.get('/categories', categoriesController.getCategories)
router.get('/categories/:id', categoriesController.getOneCategory)
router.put('/categories', categoriesController.updateCategory)
router.delete('/categories/:id', categoriesController.deleteCategory)

export default router

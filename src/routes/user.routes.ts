import express from 'express'
import checkAuth from '../utils/checkAuth'
import userController from '../controller/user.controller'

const router = express.Router()

router.post('/user/', userController.createUser)
router.post('/user/auth', userController.authUser)
router.post('/user/me', userController.myUser)
router.get('/user', userController.getUsers)
router.get('/user/:id', userController.getOneUser)
router.put('/user', userController.updateUser)
router.delete('/user/:id', checkAuth, userController.deleteUser)

export default router
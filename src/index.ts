import express from 'express'
import Logging from './library/Logging'
import cors from 'cors'
import passport from 'passport'
import cookieParser from 'cookie-parser'
import session from 'express-session'
import http from 'http';
import { config } from './config/config';
import { checkAuth } from './utils'
import multer from 'multer'
import fs from 'fs';

import jwt from 'jsonwebtoken'

// const userRouter = require('./routes/user.routes')
import postRouter from './routes/post.routes'
import productRouter from './routes/product.routes'
// const categoriesRouter = require('./routes/categories.routes')

const app = express()

const storage = multer.diskStorage({
  destination: (_, __, cb) => {
    if (!fs.existsSync('uploads')) {
      fs.mkdirSync('uploads');
    }
    cb(null, 'uploads');
  },
  filename: (_, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });

app.use(express.json())
app.use(cors({ origin: 'http://localhost:3000', credentials: true }))
app.use(
  session({
    secret: "secretcode",
    resave: true,
    saveUninitialized: true,
  }))
app.use(cookieParser())
app.use(passport.initialize())
app.use(passport.session())

const StartServer = () => {
  app.use((req, res, next) => {
    Logging.info(`[${req.method}] - URL: [${req.url}] - IP: [${req.socket.remoteAddress}]`);

    res.on('finish', () => {
      Logging.info(`[${req.method}] - 
        URL: [${req.url}] - IP: [${req.socket.remoteAddress}] - STATUS: [${res.statusCode}]`);
    });

    next();
  });

  app.use(express.urlencoded({ extended: true }))

  // app.use('/api', userRouter)
  app.use('/api', postRouter)
  app.use('/api', productRouter)
  // app.use('/api', categoriesRouter)

  app.use('/uploads', express.static('uploads'));

  app.post('/upload', checkAuth, upload.single('image'), (req, res) => {
    res.json({
      url: `/uploads/${req.file?.originalname}`,
    });
  });

  http.createServer(app)
    .listen(config.server.port,
      () => Logging.info(`Сервер запущен на порту ${config.server.port}`));
}

StartServer()
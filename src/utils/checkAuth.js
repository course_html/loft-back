import jwt from 'jsonwebtoken'

export default (req, res, next) => {
  // ? replace(/Bearer\s?/, '') регулярка удаляет слово Bearer из токена
  // ? и возвращает пустую строку вместо этого слова

  const token = (req.headers.authorization || '').replace(/Bearer\s?/, '')

  if (token) { 
    try {
      const decoded = jwt.verify(token, 'secret key')

      req.userId = decoded._id

      next()
    } catch (e) {
      return res.status(403).json({
        message: 'invalid token',
      })
    }
  } else {
    return res.status(403).json({
      message: 'invalid token',
    })
  }
}

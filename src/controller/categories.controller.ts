
const db = require('../db')
import { Request, Response } from 'express';

class CategoriesController {

	async createCategory(req: Request, res: Response) {
		const { name, img, link } = req.body
		const newCategory = await db.query(
			`INSERT INTO categories ( name, img, link) values ($1, $2, $3) RETURNING *`,
			[ name, img, link ]
		)
		res.json(newCategory.rows[0])
	}

	async getCategories(req: Request, res: Response) {
		const categories = await db.query(`SELECT * FROM categories`)
		res.json(categories.rows)
	}

	async getOneCategory(req: Request, res: Response) {
		const id = req.params.id
		const category = await db.query(`SELECT * FROM categories where id = $1`, [ id ])
		res.json(category.rows[0])
	}

	async updateCategory(req: Request, res: Response) {
		const { name, img, link, id } = req.body
		const updateCategory = await db.query(
			`UPDATE categories set name = $1, img = $2, link = $3, where id = $4 RETURNING *`,
			[ name, img, link, id ]
		)
		res.json(updateCategory.rows[0])
	}

	async deleteCategory(req: Request, res: Response) {
		const id = req.params.id
		const category = await db.query('DELETE FROM categories where id = $1', [ id ])
		res.json(category.rows[0])
	}
}

// module.exports = new CategoriesController()
export default new CategoriesController()

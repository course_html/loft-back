const db = require('./../db')
import { Request, Response } from 'express';

class PostController {
    async createPost(req: Request, res: Response) {
        const {title, content, user_id} = req.body
        console.log(user_id)
        const newPost = await db.query(`INSERT INTO post (title, content, user_id) values ($1, $2, $3) RETURNING *`, [title, content, user_id])
        res.json(newPost.rows[0])
    }

    async getPostsByUser(req: Request, res: Response) {
        const id = req.query.id
        console.log(id)
        const posts = await db.query('select * from post where user_id = $1', [id])
        res.json(posts.rows)
    }
}

// module.exports = new PostController()
export default new PostController()
const db = require('../db')
import bcrypt from 'bcryptjs'
const jwt = require('jsonwebtoken')
import { Request, Response, NextFunction } from 'express';

class UserController {
  async myUser(req: Request, res: Response, next: NextFunction) {
    try {
    const { name } = req?.body
     const user = await db.query(`SELECT * FROM users WHERE name = $1`, [
      name,
    ])

    if (!user.rows[0]) {
      res.json({ 
       message: 'Пользователь не найден'
     })
     return
   }

   res.json({...user.rows[0]})

    } catch (error) {
      console.log(error);
    }
  }

  async authUser(req: Request, res: Response, next: NextFunction) {
    try {
      const { name, password } = req?.body

      const newUser = await db.query(`SELECT * FROM users WHERE name = $1`, [
        name,
      ])

      if (!newUser.rows[0]) {
         res.json({ 
          message: 'хренова друг, таких тут нет'
        })
        return
      }

      const isValidPass = await bcrypt.compare(password, newUser.rows[0].password)

      if (!isValidPass) {
        res.json({ 
         message: 'пароль забыл?'
       })
       return
     }

      const token = jwt.sign({ _id: newUser.rows[0].id }, 'secret key', { expiresIn: '30d' })

      res.json({
        ...newUser.rows[0],
        token,
      })
    } catch (error) {
      console.log(error);
    }
  }

  async createUser(req: Request, res: Response) {
    try {
      const { name, password } = req?.body
      const salt = await bcrypt.genSalt(10)
      const hashPassword = await bcrypt.hash(password, salt)

      if (
        (!name || !password &&
        typeof hashPassword !== 'string' ||
          typeof name !== 'string' ||
          name.length < 3)
      ) {
        res.send('ай-яй, давайка нормальнO заполняй')
        return
      }
      const newUser = await db.query(
        `INSERT INTO users (name, password) values ($1, $2) RETURNING *`,
        [name, hashPassword]
      )

      const token = jwt.sign({ _id: newUser.rows[0].id }, 'secret key', {
        expiresIn: '30d',
      })

      res.json({
        ...newUser.rows[0],
        token,
      })
    } catch (error) {
      console.log(error)
      res.status(500).json({ message: 'не удалось зарегестрироваться' })
    }
  }

  async getUsers(req: Request, res: Response) {
    const users = await db.query(`SELECT * FROM users`)
    res.json(users.rows)
    res.send('отдал')
  }

  async getOneUser(req: Request, res: Response) {
    const id = req.params.id
    const user = await db.query(`SELECT * FROM users where id = $1`, [id])
    res.json(user.rows[0])
  }

  async updateUser(req: Request, res: Response) {
    const { id, email, password } = req.body
    const updateUser = await db.query(`UPDATE users set name = $1, password = $2 where id = $3 RETURNING *`, [email, password, id])
      
    res.json(updateUser.rows[0])
  }

  async deleteUser(req: Request, res: Response) {
    const id = req.params.id
    const user = await db.query('DELETE FROM users where id = $1', [id])
    // TODO узнать как проверить есть ли в базе такой ID и вернуть сообщение если таких нет
    // if (!id) {

    // }
    res.send(`успешно удален пользователь с ID${id}`)
    res.json(user.rows[0])
  }
}

export default new UserController()

const db = require('../db')
import { Request, Response } from 'express';

class ProductController {

	async createProduct(req: Request, res: Response) {
		const { title, price, image, oldPrice, sale, description } = req.body
		const newProduct = await db.query(
			`INSERT INTO product (title, price, image, oldPrice, sale, description) values ($1, $2, $3, $4, $5, $6) RETURNING *`,
			[ title, price, image, oldPrice, sale, description ]
		)
		res.json(newProduct.rows[0])
	}

	async getProducts(req: Request, res: Response) {
		const products = await db.query(`SELECT * FROM product`)
		res.json(products.rows)
	}

	async getOneProduct(req: Request, res: Response) {
		const id = req.params.id
		const product = await db.query(`SELECT * FROM product where id = $1`, [ id ])
		res.json(product.rows[0])
	}

	async updateProduct(req: Request, res: Response) {
		const { title, price, image, oldPrice, sale, description, id } = req.body
		const updateProduct = await db.query(
			`UPDATE product set title = $1, price = $2, image = $3, oldPrice = $4, sale = $5, description = $6 where id = $7 RETURNING *`,
			[ title, price, image, oldPrice, sale, description, id ]
		)
		res.json(updateProduct.rows[0])
	}

	async deleteProduct(req: Request, res: Response) {
		const id = req.params.id
		const product = await db.query('DELETE FROM product where id = $1', [ id ])
		res.json(product.rows[0])
	}
}

export default new ProductController()

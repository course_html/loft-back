create TABLE person(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    surname VARCHAR(255)
)

create TABLE post(
    id SERIAL PRIMARY KEY,
    title VARCHAR(255),
    content VARCHAR(255),
    user_id INTEGER,
    FOREIGN KEY (user_id) REFERENCES person (id)
)

create TABLE categories(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    img VARCHAR(255),
    link VARCHAR(255),
)